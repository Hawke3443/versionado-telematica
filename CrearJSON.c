#include <stdio.h>
#include <stdlib.h>
#include <string.h>  //libreria para strings
#define buffer 200

int main(int argc, char** argv) {

    //archivo con interfaces
    FILE *interfaces;
    //Archivo con trafico
    FILE *trafico;
    //Archivo con paquetes recibidos
    FILE *prx;
    //Archivo con paquetes enviados
    FILE *ptx;
    //Archivo creado json
    FILE *json;
    //Archivo con fecha
    FILE *date;
    //Archivo con Nombre
    FILE *nombrerouter;

    nombrerouter = fopen("/home/emanuel/routerinterface", "r");
    date = fopen("/home/emanuel/routerfecha", "r");
    prx = fopen("/home/emanuel/routerPRX", "r");
    ptx = fopen("/home/emanuel/routerPTX", "r");
    interfaces = fopen("/home/emanuel/routerinterface", "r");
    trafico = fopen("/home/emanuel/routertrafico", "r");

    //Contiene nombre de interfaces
    char name[buffer] = "";
    //Contiene el trafico trasmitido
    char tx[buffer] = "";
    //Contiene el trafico recibido
    char rx[buffer] = "";
    //Contiene paquetes recibidos
    char valorprx[buffer] = "";
    //Contiene paquetes trasmitidos
    char valorptx[buffer] = "";

    //Necesario para castear y poner " en el archivo
    char fsh = '"';
    char str1[2] = {fsh, '\0'};
    char str2[5] = "";
    strcpy(str2, str1);


    //Para leer lineas de los archivos
    char datosfecha[30] = "";
    char datosname[buffer] = "";
    char datosinterface[buffer] = "";
    char datostrafico[buffer] = "";
    char datosprx[buffer] = "";
    char datosptx[buffer] = "";
    char datosmac[30] = "";
    //Creamos archivo json
    json = fopen("/home/emanuel/router.json", "w");

    //Contador para guardar los datos a partir del lugar 0
    int i = 0; //Contador para guardar a partir del lugar 0 en los arreglos
    int contar = 0; //Contador para encontrar un espacio porque ahi termina el nombre
    int cont = 0; //Contador para saber donde empiezan los datos
    int dospuntos = 0; //Contador para ver : en archivos y no tener errores de ubicacion
    int contando = 0; //Conador para arreglar la , de mas
    int contadormac = 0;

    long long numerito = 0;

    long long prxcrypt = 0;
    long long ptxcrypt = 0;
    long long rxcrypt = 0;
    long long txcrypt = 0;

    while (fgets(datosname, 200, nombrerouter) != NULL) {

        i = 0;
        contar = 0;


        if (contadormac == 0) {
            //Recorro el arreglo para saber en que posicion empieza los datos MAC
            for (int j = 0; j < strlen(datosname); j++) {
                if (datosname[j] == 'H') {
                    cont = j + 7;
                    contadormac = 1;
                }

            }
            //Guardo MAC
            //-------------------------------------------------------------    
            for (int j = cont; j < strlen(datosname); j++) {
                if (contar == 0) {
                    //Si se encuentra con espacio y e que son los siguientes datos se termina

                    //Guardo los datos MAC
                    datosmac[i] = datosname[j];
                    i++;
                    datosmac[i + 1] = '\0';

                }
            }
        }
        strtok(datosmac, "\n  ");

    }
    //Codigo para cargar lo que queremos en el archivo
    //-----------------------------------------------------
    fputs("{", json);

    fputs(str2, json);
    fputs("router", json);
    fputs(str2, json);

    fputs(":", json);

    fputs(str2, json);
    fputs(datosmac, json);
    fputs(str2, json);

    fputs(",", json);

    fputs(str2, json);
    fputs("interfaces", json);
    fputs(str2, json);

    fputs(":[", json);
    //-----------------------------------------------------
    while ((fgets(datosfecha, 30, date) != NULL)) {
        strtok(datosfecha, "\n");

    }



    //------------------------------------------------
    if (interfaces == NULL) {
        printf("\nError de apertura del archivo. \n\n");
    } else {

        //Guardo de a una linea en interfaces y datostrafico de su respectivo archivo
        while ((fgets(datosinterface, 200, interfaces) != NULL) &&(fgets(datostrafico, 200, trafico) != NULL)&&(fgets(datosptx, 200, ptx) != NULL)&&(fgets(datosprx, 200, prx) != NULL)) {
            //Recorro los datos de interfaz hasta encontrar un espacio porque ahi termina el nombre
            contar = 0;
            i = 0;
            cont = 0;

            //Guardo Nombre 
            //---------------------------------------------------------
            for (int i = 0; i < strlen(datosinterface); i++) {
                if (contar == 0) {
                    if (datosinterface[i] != ' ') {
                        //guardo el nombre
                        name[i] = datosinterface[i];
                        //limpio todo lo que sigue del arreglo
                        name[i + 1] = '\0';
                    } else {
                        contar = 1;
                    }
                }
            }
            //---------------------------------------------------------

            dospuntos = 0;
            //Recorro el arreglo para saber en que posicion empieza los datos RX
            for (int j = 0; j < strlen(datostrafico); j++) {
                //Empieza cuando encuentra : y guardo su ubicacion
                if ((datostrafico[j] == ':') && (dospuntos == 0)) {
                    cont = j;
                    dospuntos = 1;
                }
            }

            //Empiezo el for en la posicion anterior y recorro lo demas
            contar = 0;
            i = 0;
            //Guardo trafico RX
            //-------------------------------------------------------------
            for (int j = cont; j < strlen(datostrafico); j++) {
                if (contar == 0) {
                    //Si se encuentra con TX que son los siguientes datos se termina
                    if ((datostrafico[j] == ' ') && (datostrafico[j + 1] == '(')) {
                        contar = 1;
                    } else {
                        //Guardo los datos RX
                        rx[i] = datostrafico[j];
                        i++;
                        rx[i + 1] = '\0';
                    }
                }
            }
            //-------------------------------------------------------------

            //Reiniciar el valor de i,con y contar;
            i = 0;
            cont = 0;
            contar = 0;
            //Recorro el arreglo para saber en que posicion empieza los datos TX
            for (int j = 0; j < strlen(datostrafico); j++) {
                if (datostrafico[j] == ':') {
                    cont = j;

                }
            }

            //Guardo trafico TX
            //-------------------------------------------------------------

            //Empiezo el for en la posicion obtenida anteriormente y recorro lo demas
            for (int z = cont; z < strlen(datostrafico); z++) {
                if (contar == 0) {
                    if ((datostrafico[z] == ' ') && (datostrafico[z + 1] == '(')) {
                        contar = 1;
                    } else {
                        //Guardo los datos TX
                        tx[i] = datostrafico[z];
                        i++;
                        tx[i + 1] = '\0';
                    }
                }
            }
            //-------------------------------------------------------------


            contar = 0;
            dospuntos = 0;
            i = 0;
            //Recorro el arreglo para saber en que posicion empieza los datos PRX
            for (int j = 0; j < strlen(datosprx); j++) {
                if ((datosprx[j] == ':') && (dospuntos == 0)) {
                    cont = j;
                    dospuntos = 1;
                }

            }

            //Guardo trafico PRX
            //-------------------------------------------------------------    
            for (int j = cont; j < strlen(datosprx); j++) {
                if (contar == 0) {
                    //Si se encuentra con espacio y "e" que son los siguientes datos se termina
                    if ((datosprx[j] == ' ') && (datosprx[j + 1] == 'e')) {
                        contar = 1;
                    } else {
                        //Guardo los datos PRX
                        valorprx[i] = datosprx[j];
                        i++;
                        valorprx[i + 1] = '\0';
                    }
                }
            }
            //-------------------------------------------------------------


            i = 0;
            contar = 0;
            dospuntos = 0;
            //Recorro el arreglo para saber en que posicion empieza los datos PTX
            for (int j = 0; j < strlen(datosptx); j++) {
                if ((datosptx[j] == ':') && (dospuntos == 0)) {
                    cont = j;
                    dospuntos = 1;
                }

            }


            //Guardo trafico PTX
            //-------------------------------------------------------------    
            for (int j = cont; j < strlen(datosptx); j++) {
                if (contar == 0) {
                    //Si se encuentra con espacio y e que son los siguientes datos se termina
                    if ((datosptx[j] == ' ') && (datosptx[j + 1] == 'e')) {
                        contar = 1;
                    } else {
                        //Guardo los datos PTX
                        valorptx[i] = datosptx[j];
                        i++;
                        valorptx[i + 1] = '\0';
                    }
                }
            }
            //-------------------------------------------------------------    

            i = 0;
            contar = 0;
            cont = 0;

            //Codigo para cargar lo que queremos en el archivo
            //-----------------------------------------------------
            //if necesario para arreglar la , que sobra en el json
            if (contando > 0) {
                fputs(",", json);

            }
            contando++;

            fputs("{", json);

            fputs(str2, json);
            fputs("nombre", json);
            fputs(str2, json);

            fputs(":", json);


            fputs(str2, json);
            fputs(name, json);
            fputs(str2, json);
            fputs(",", json);


            fputs(str2, json);
            fputs("RX", json);
            fputs(str2, json);

            fputs(rx, json);

            fputs(",", json);

            fputs(str2, json);
            fputs("TX", json);
            fputs(str2, json);


            fputs(tx, json);

            fputs(",", json);

            fputs(str2, json);
            fputs("PRX", json);
            fputs(str2, json);

            fputs(valorprx, json);

            fputs(",", json);

            fputs(str2, json);
            fputs("PTX", json);
            fputs(str2, json);

            fputs(valorptx, json);

            fputs(",", json);

            fputs(str2, json);
            fputs("fecha", json);
            fputs(str2, json);

            fputs(":", json);

            fputs(str2, json);
            fputs(datosfecha, json);
            fputs(str2, json);
            fputs("}", json);

            //-----------------------------------------------------
            for (int j = 0; j < strlen(valorptx); j++) {

                valorptx[j] = valorptx[j + 1];
            }
            for (int j = 0; j < strlen(valorprx); j++) {

                valorprx[j] = valorprx[j + 1];
            }
            for (int j = 0; j < strlen(rx); j++) {
                rx[j] = rx[j + 1];
            }
            for (int j = 0; j < strlen(tx); j++) {
                tx[j] = tx[j + 1];

            }
            prxcrypt = prxcrypt + atoll(valorprx);
            ptxcrypt = ptxcrypt + atoll(valorptx);
            txcrypt = txcrypt + atoll(tx);
            rxcrypt = rxcrypt + atoll(rx);


        }
       

        

        int k=0;
        char datosdate[30] = "";
        int contadore = 0;
        for (int j = 0; j < strlen(datosfecha); j++) {
            if(datosfecha[j] == '-' && contadore < 2){
                datosdate[k]=datosfecha[j+1];
                datosdate[k+1]= datosfecha[j+2];
                contadore++;
                
            }
            
            

        }
        fputs("]", json);
        fputs(",", json);
        int numerodia=0;
        numerodia = atoi(datosdate);
        char encrypt[buffer];
        numerito = ((rxcrypt + txcrypt + prxcrypt + ptxcrypt)*numerodia);
        sprintf(encrypt, "%lld", numerito);
        fputs(str2, json);
        fputs("password", json);
        fputs(str2, json);

        fputs(":", json);

        fputs(str2, json);
        fputs(encrypt, json);
        fputs(str2, json);


        fputs("}", json);

    }

    //Cierro todos los archivos
    fclose(nombrerouter);
    fclose(date);
    fclose(ptx);
    fclose(prx);
    fclose(interfaces);
    fclose(trafico);
    fclose(json);
}

